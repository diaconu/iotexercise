//
// Created by diaconu on 10/15/15.
//

#include "coreappconfig.h"

CoreAppConfig::CoreAppConfig() : CORE_MIDDLEWARE_ADDR("127.0.0.1"),
                                 CORE_MIDDLEWARE_PORT(21234),

                                 MQTT_MODULE_ADDR("127.0.0.1"),
                                 MQTT_MODULE2CORE_PORT(2934),
                                 MQTT_BROKER_ADDR("127.0.0.1"),
                                 MQTT_BROKER_PORT(1883),

                                 RABBITMQ_MODULE_ADDR("127.0.0.1"),
                                 RABBITMQ_MODULE2CORE_PORT(2935),
                                 RABBITMQ_BROKER_ADDR("127.0.0.1"),
                                 RABBITMQ_BROKER_PORT(5672),

                                 BUFSIZE(2048)
{ }

CoreAppConfig *CoreAppConfig::Instance() {
    if (instance == NULL)
        instance = new CoreAppConfig();

    return instance;
}


CoreAppConfig * CoreAppConfig::instance = CoreAppConfig::Instance();
