//
// Created by diaconu on 10/15/15.
//

#ifndef COREMIDDLEWARE_LOADCONFIG_H
#define COREMIDDLEWARE_LOADCONFIG_H

#include <stdio.h>

/*
 * This class mocks a config loader.
 * The values are now hard coded in the constructor.
 * Change them appropriately according to fit system.
 */
class CoreAppConfig
{
private:
    static CoreAppConfig * instance;

    CoreAppConfig();

public:
    const char* CORE_MIDDLEWARE_ADDR;
    const int CORE_MIDDLEWARE_PORT;

    const char* MQTT_MODULE_ADDR;
    const int MQTT_MODULE2CORE_PORT;
    const char* MQTT_BROKER_ADDR;
    const int MQTT_BROKER_PORT;

    const char* RABBITMQ_MODULE_ADDR;
    const int RABBITMQ_MODULE2CORE_PORT;
    const char* RABBITMQ_BROKER_ADDR;
    const int RABBITMQ_BROKER_PORT;

    const int BUFSIZE;

    static CoreAppConfig * Instance();
};

#endif //COREMIDDLEWARE_LOADCONFIG_H
