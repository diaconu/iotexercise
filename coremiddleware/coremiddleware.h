//
// Created by diaconu on 10/15/15.
//

//TODO add event queue

#ifndef COREMIDDLEWARE_COREMIDDLEWARE_H
#define COREMIDDLEWARE_COREMIDDLEWARE_H

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include <list>
#include "../config/coreappconfig.h"

using boost::asio::ip::udp;

/*
 * Module data for connection,
 * aka, where to send messages
 */
struct Module
{
    boost::asio::ip::udp::endpoint destination;
};


/*
 * A very simple server class
 */
class CoreMiddleware
{

public:
    static CoreMiddleware * Instance();;

private:

    static std::list<Module*> *modules;
    static CoreMiddleware *cm;
    static CoreAppConfig cfg;

    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    char recv_buffer_[2048];

    CoreMiddleware(boost::asio::io_service& io_service);
    ~CoreMiddleware();

    /*
     * Blocking function waiting for a message from a module.
     */
    void start_receive();

    /*
     * Deferred to handle incoming messages.
     * Parse the stream, call the appropriate do_ function.
     */
    void handle_receive(const boost::system::error_code& error,  std::size_t len_buffer_);

    /*
     * Deferred to handle sent errors.
     */
    void handle_send();


    /*
     * The following functions do some basic message parsing.
     * They do not check for errors.
     */
    void do_add_module(int port, std::string addr);

    /*
     * Message parsing: increment number, concatenate to the string
     */
    void do_receive(int nb, std::string data);

    void do_send(int nb, std::string data);
};

#endif //COREMIDDLEWARE_COREMIDDLEWARE_H
