//
// Created by diaconu on 10/15/15.
//

#include "coremiddleware.h"

CoreMiddleware *CoreMiddleware::Instance() {
    if (!cm)   // Only allow one instance of class to be generated.
    {
        std::cout << "Starting Core Middleware " << std::endl;
        boost::asio::io_service io_service;
        cm = new CoreMiddleware(io_service);
        modules = new std::list<Module *>();
        //cfg = CoreAppConfig();

        io_service.run();
        std::cout << "Starting Core Middleware " << std::endl;
    }
    return cm;
}

CoreMiddleware::CoreMiddleware(boost::asio::io_service& io_service) : socket_(io_service, udp::endpoint(udp::v4(),  cfg.CORE_MIDDLEWARE_PORT)) {
    //recv_buffer_= new char[cfg.BUFSIZE];
    start_receive();
}

void CoreMiddleware::start_receive() {
    socket_.async_receive_from(
            boost::asio::buffer(recv_buffer_), remote_endpoint_,
            boost::bind(&CoreMiddleware::handle_receive, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
}

void CoreMiddleware::handle_receive(const boost::system::error_code& error,  std::size_t len_buffer_/*bytes_transferred*/) {
    if (!error || error == boost::asio::error::message_size)
    {
        std::cout << "Received message: " << recv_buffer_ << std::endl;
        //end the string
        recv_buffer_[len_buffer_]='\0';

        bool malformed = false;
        if (len_buffer_>7)
        {
            char *token = strtok (recv_buffer_," ");
            char * method = token;// extract method: a or r

            int nb = 0;
            try {
                token = strtok (NULL, " ");
                nb = std::atoi(token);     // get the number in str
            }catch (...){
                malformed = true;
            }
            token = strtok (NULL, "");
            std::string data(token);           // get the rest of the message

            //do method
            if(method[0] == 'a')
                do_add_module(nb, data);
            else if (method[0] == 'm')
                do_receive(nb, data);
            else
                malformed = true;
        }
        else
            malformed= true;

        if (malformed)
        {
            std::cerr << "ERROR:  malformed message structure" << std::endl;
        }

        start_receive();
    }
    else
    {
        std::cerr << "ERROR: at message reception" << std::endl;
    }
}

void CoreMiddleware::handle_send() {
}

CoreMiddleware::~CoreMiddleware() {
    //delete(modules);
    //delete(cm);
}

void CoreMiddleware::do_add_module(int port, std::string addr) {
    std::cout << "\tadd module addr: " << addr << " port: " << port << std::endl;
    Module *mod = new Module;
    mod->destination = boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(addr), port);
    CoreMiddleware::modules->push_back(mod);

    std::cout << "\t" << CoreMiddleware::modules->size() << "  module(s) connected" << std::endl;

    //test connection
    char send_buffer_[]="welcome";
    int len_buffer_=std::strlen(send_buffer_);

    socket_.async_send_to(
            boost::asio::buffer(send_buffer_,len_buffer_), mod->destination,
            boost::bind(&CoreMiddleware::handle_send, this));
}

void CoreMiddleware::do_receive(int nb, std::string data) {
    std::cout << "Parsed message, number: " << nb << " message: " << data << std::endl;

    // data modification
    std::string str_nb = std::to_string(++nb);
    data.append(str_nb);

    do_send(nb, data);
}

void CoreMiddleware::do_send(int nb, std::string data) {
    std::cout << "Reply to all, number: " << nb << ", message: " << data << std::endl;

    char send_buffer_[cfg.BUFSIZE];
    strcpy(send_buffer_, std::to_string(nb).c_str());
    strcat(send_buffer_, " ");
    strcat(send_buffer_, data.c_str());

    //std::cout << send_buffer_ << std::endl;
    int len_buffer_=std::strlen(send_buffer_);

    for(std::list<Module*>::iterator it=CoreMiddleware::modules->begin() ; it!=CoreMiddleware::modules->end() ; ++it)
    {
        Module* tmp = *it;

        std::cout << "\tsending to: (" << tmp->destination.address().to_v4() << ":" << tmp->destination.port() << ")" << std::endl;

        socket_.async_send_to(
                boost::asio::buffer(send_buffer_,len_buffer_), tmp->destination,
                boost::bind(&CoreMiddleware::handle_send, this));

        /*socket_.async_send_to(boost::asio::buffer((void*)send_buffer_, strlen(send_buffer_)), remote_endpoint_,
                              boost::bind(&CoreMiddleware::handle_send, this, send_buffer_,
                                          boost::asio::placeholders::error,
                                          boost::asio::placeholders::bytes_transferred));
                                          */
    }

}

CoreMiddleware* CoreMiddleware::cm = NULL;
std::list<Module*>* CoreMiddleware::modules = NULL;
CoreAppConfig CoreMiddleware::cfg = *(CoreAppConfig::Instance());
