//
// Created by diaconu on 10/18/15.
//

#include <iostream>
#include <thread>

#include "rabbitmqconnect.h"

using namespace std;

RabbitmqConnect *ps;

static void* get_input()
{
    while(true)
    {
        char message[2048];
        std::cin.getline(message, 2048, '\n') ;
        cout << " read " << message << endl;

        ps->send_message(message);
    }
}

static void* run_loop()
{
    ps->run();
}

int main() {

    ps = new RabbitmqConnect();

    std::thread t1(get_input);
    std::thread t2(run_loop);


    t1.join();
    t2.join();


    return 0;
}