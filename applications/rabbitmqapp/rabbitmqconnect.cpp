//
// Created by diaconu on 10/17/15.
//

#include "rabbitmqconnect.h"


void *RabbitmqConnect::init_subscriber(void *) {
    conn_receive = amqp_new_connection();
    socket_receive = amqp_tcp_socket_new(conn_receive);
    if (amqp_socket_open(socket_receive, "127.0.0.1", 5672)) {
        std::cerr << "Error connecting the subscriber to the RabbitMQ broker. Does it run? " << std::endl;
    }

    amqp_login(conn_receive, "/", 0, 131072, 0, AMQP_SASL_METHOD_PLAIN, "guest", "guest");
    amqp_channel_open(conn_receive, 1);
    amqp_get_rpc_reply(conn_receive);

    {
        amqp_queue_declare_ok_t *r = amqp_queue_declare(conn_receive, 1, amqp_empty_bytes, 0, 0, 0, 1,
                                                        amqp_empty_table);
        queuename = amqp_bytes_malloc_dup(r->queue);
        if (queuename.bytes == NULL) {
            std::cerr << "Out of memory while copying queue name" << std::endl;
            return NULL;
        }
    }

    amqp_queue_bind(conn_receive, 1, queuename, amqp_cstring_bytes("amq.direct"), amqp_cstring_bytes("receive queue"), amqp_empty_table);
    amqp_get_rpc_reply(conn_receive);

    amqp_basic_consume(conn_receive, 1, queuename, amqp_empty_bytes, 0, 1, 0, amqp_empty_table);
    amqp_get_rpc_reply(conn_receive);

}


void *RabbitmqConnect::init_publisher(void *) {
    conn_send = amqp_new_connection();
    socket_send = amqp_tcp_socket_new(conn_send);
    if (amqp_socket_open(socket_send, "127.0.0.1", 5672)) {
        std::cerr << "Error connecting the publisher to the RabbitMQ broker. Does it run? " << std::endl;
    }
    amqp_login(conn_send, "/", 0, 131072, 0, AMQP_SASL_METHOD_PLAIN, "guest", "guest");
    amqp_channel_open(conn_send, 1);
    amqp_get_rpc_reply(conn_send);
}

void RabbitmqConnect::subscriber_loop() {
    std::cout << "Listening for incomming RabbitMQ messages" <<  std::endl;
    while (true)
    {
        amqp_envelope_t envelope;
        amqp_maybe_release_buffers(conn_receive);
        amqp_consume_message(conn_receive, &envelope, NULL, 0);

        std::cout << "Received message from RabbitMQ broker: " << (char*)envelope.message.body.bytes << std::endl;

    }
}

RabbitmqConnect::RabbitmqConnect() {
    std::cout << "Plugging RabbitMQ module " << std::endl;
    init_publisher(NULL);

    init_subscriber(NULL);
    //pthread_t t;
    //pthread_create(&t, NULL, subscriber_loop, (void *)(&conn_receive));
};

void RabbitmqConnect::run(){
    subscriber_loop();
}

bool RabbitmqConnect::send_message(char *_message) {
    std::cout << "Send message to RabbitMQ module: " << _message << std::endl;

    int ret = amqp_basic_publish(conn_send,
                                 1,
                                 amqp_cstring_bytes("amq.direct"),
                                 amqp_cstring_bytes("send queue"), //TOPIC
                                 0,
                                 0,
                                 NULL, //&props,
                                 amqp_cstring_bytes(_message));

    return (ret == AMQP_RESPONSE_NORMAL);
}