//
// Created by diaconu on 10/17/15.
//

#ifndef APPLICATIONS_RABBITMQCONNECT_H
#define APPLICATIONS_RABBITMQCONNECT_H

#include <boost/asio/ip/udp.hpp>

#include <amqp_tcp_socket.h>
#include <amqp.h>
#include <amqp_framing.h>

using boost::asio::ip::udp;

class RabbitmqConnect//:public Module
{
private:

    //connection data
    amqp_socket_t *socket_send = NULL, *socket_receive = NULL;
    amqp_connection_state_t conn_send, conn_receive;

    amqp_bytes_t queuename;

    void * init_subscriber(void *);;

    void * init_publisher(void *);;

    void subscriber_loop();;

public:

    RabbitmqConnect();;

    ~RabbitmqConnect()
    {
        //conn_receive->close();
        //conn_send->close();
    };

    //forward incomming messages from the CoreMiddleware
    //to be called from the RabbitMq2Core at message arrival
    bool send_message(char *_message);;

    void run();


};


#endif //APPLICATIONS_RABBITMQCONNECT_H
