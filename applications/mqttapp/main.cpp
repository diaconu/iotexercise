//
// Created by diaconu on 10/16/15.
//

#include <iostream>
#include <thread>


using namespace std;

#include <string.h>

static void* get_input()
{
    while(true)
    {
        char message[2048];
        std::cin.getline(message, 2048, '\n') ;

        char command [2048] =  "mosquitto_pub -t \"0\" -m \"";
        strcat(command, message);
        strcat(command, "\"");
        system(command);
    }
}

int main() {

    std::thread t1(get_input);

    system("mosquitto_sub -t \"1\"");

    t1.join();


    return 0;
}