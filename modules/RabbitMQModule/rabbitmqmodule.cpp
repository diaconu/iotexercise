//
// Created by diaconu on 10/16/15.
//

#include "rabbitmqmodule.h"


RabbitmqModule * RabbitmqModule::module = NULL;

void RabbitmqModule::run() {
    io_service.run();
}

RabbitmqModule::RabbitmqModule( void (*_handler)(char*)) {
    r2c = new Module2Core(io_service, CoreAppConfig::Instance()->RABBITMQ_MODULE2CORE_PORT, rabbitmq_handler);
    r2a = new Rabbitmq2Apps(r2c);
    r2c->handler = rabbitmq_handler;

    //consummer is blocking,
    //need to run the communication with the core in a different thread
    pthread_t t;
    pthread_create(&t, NULL, module2broker, NULL);
}


void RabbitmqModule::rabbitmq_handler(char *msg) {
    //std::cout << " in mqtt_handler " << msg << std::endl;
    module->r2a->send_message(msg);
}

RabbitmqModule::~RabbitmqModule() {
    io_service.stop();
    //delete(r2a);
    //delete(r2c);
}

RabbitmqModule *RabbitmqModule::Instance() {
    if (!module)   // Only allow one instance of class to be generated.
    {
        module = new RabbitmqModule(RabbitmqModule::rabbitmq_handler);
    }
    return module;
}