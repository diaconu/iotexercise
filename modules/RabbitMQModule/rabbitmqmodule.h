//
// Created by diaconu on 10/16/15.
//

#ifndef MODULES_RABBITMQMODULE_H
#define MODULES_RABBITMQMODULE_H

#include <boost/asio/io_service.hpp>
#include "../../config/coreappconfig.h"
#include "../common/module2core.h"
#include "rabbitmq2apps.h"


#include <pthread.h>

class RabbitmqModule//:public Module
{
private:
    static RabbitmqModule *module;
    Rabbitmq2Apps *r2a;
    Module2Core * r2c;

    RabbitmqModule( void (*_handler)(char*));;

public:
    static void rabbitmq_handler(char *msg);

    boost::asio::io_service io_service;


    ~RabbitmqModule();

    //attaching a handler for the messages coming from the core midleware
    //it calls the bradoadcasting through the broker
    static void* module2broker(void* )
    {

        module->r2a->init_thread();
    }

    void run();

    static RabbitmqModule * Instance();
};


#endif //MODULES_RABBITMQMODULE_H
