//
// Created by diaconu on 10/16/15.
//

#include "rabbitmqmodule.h"

int main()
{
    RabbitmqModule * mod = RabbitmqModule::Instance();
    mod->run();

    return 0;
}