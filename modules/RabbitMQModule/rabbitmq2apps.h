//
// Created by diaconu on 10/16/15.
//

#ifndef MODULES_RABBITMQ2APPS_H
#define MODULES_RABBITMQ2APPS_H

#include <boost/asio/ip/udp.hpp>

#include <amqp_tcp_socket.h>
#include <amqp.h>
#include <amqp_framing.h>
#include "../../config/coreappconfig.h"
#include "../common/module2core.h"

using boost::asio::ip::udp;

class Rabbitmq2Apps//:public Module
{
private:

    Module2Core *r2c;

    //connection data
    amqp_socket_t *socket_send = NULL, *socket_receive = NULL;
    amqp_connection_state_t conn_send, conn_receive;

    amqp_bytes_t queuename;

    void * init_subscriber(void *);;

    void * init_publisher(void *);;

    void subscriber_loop();;


public:

    Rabbitmq2Apps(Module2Core *_r2c);;

    void init_thread();

    ~Rabbitmq2Apps()
    {
        //conn_receive->close();
        //conn_send->close();
    };

    //forward incomming messages from the CoreMiddleware
    //to be called from the RabbitMq2Core at message arrival
    bool send_message(char *_message);;



};

#endif //MODULES_EABBITMQ2APPS_H
