//
// Created by diaconu on 10/15/15.
//

#ifndef MODULES_MODULE2CORE_H
#define MODULES_MODULE2CORE_H


#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include <list>
#include <thread>

//TODO: simbolic link
#include "../../config/coreappconfig.h"

using boost::asio::ip::udp;


/*
 * A very simple server class
 * It connects to the CoreMiddleware API
 * To be instantiated in all modules to communicate with the CoreMiddleware
 */
class Module2Core//:public std::thread
{

public:
    Module2Core(boost::asio::io_service& io_service, int port, void (*_handler)(char*));

    ~Module2Core();

    void do_send(char * message) {
        char * send_buffer_ = message;

        socket_.async_send_to(
                boost::asio::buffer(send_buffer_,strlen(send_buffer_)), remote_endpoint_,
                boost::bind(&Module2Core::handle_send, this));
    }
    void (*handler) (char*);

private:
    udp::socket socket_;
    udp::endpoint remote_endpoint_;

    //todo change hardcoded bufsize
    char recv_buffer_[2048];

    void start_receive();

    void handle_receive(const boost::system::error_code& error,  std::size_t len_buffer_);

    void handle_send();


};


#endif //MODULES_MODULE2CORE_H
