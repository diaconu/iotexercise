//
// Created by diaconu on 10/14/15.
//

#ifndef MODULES_MODULE2APPS_H
#define MODULES_MODULE2APPS_H


/*
 * Interface for module communication to all apps connected
 * I will use it in all module classes
 */
class Module2Apps
{
private:

public:

    virtual bool on_send_message(char* msg) =0;
    virtual bool on_receive_message(char* msg) =0;

};
#endif //MODULES_MODULE2APPS_H
