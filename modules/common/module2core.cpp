//
// Created by diaconu on 10/15/15.
//

#include "module2core.h"

Module2Core::Module2Core(boost::asio::io_service& io_service, int port,  void (*_handler)(char*)) :
        socket_(io_service, udp::endpoint(udp::v4(),  port)){
    //handler = _handler;

    remote_endpoint_ = boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(CoreAppConfig::Instance()->CORE_MIDDLEWARE_ADDR ), CoreAppConfig::Instance()->CORE_MIDDLEWARE_PORT);
    start_receive();
}

void Module2Core::start_receive() {
    socket_.async_receive_from(
            boost::asio::buffer(recv_buffer_), remote_endpoint_,
            boost::bind(&Module2Core::handle_receive, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
}

void Module2Core::handle_receive(const boost::system::error_code& error,  std::size_t len_buffer_) {
    if (!error || error == boost::asio::error::message_size)
    {
        char message[CoreAppConfig::Instance()->BUFSIZE];
        strncpy(message, recv_buffer_, len_buffer_);
        message[len_buffer_] = '\0';

        std::cout << "Received message from CoreMiddleware: " << message << std::endl;
        //std::cout << "length: " << message << std::endl;
        // TODO !!! publish it to the broker

        (*handler)(message);

        start_receive();
    }
    else
    {
        std::cerr << "ERROR: at message reception from the CoreMiddleware" << std::endl;
    }
}

void Module2Core::handle_send() {
}

Module2Core::~Module2Core() {
    //socket_.shutdown();
}