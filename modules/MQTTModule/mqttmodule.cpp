//
// Created by diaconu on 10/15/15.
//

#include "mqttmodule.h"


MQTTModule * MQTTModule::module = NULL;

void MQTTModule::run() {
    io_service.run();
}

MQTTModule::MQTTModule(  void (*_handler)(char*)) {
    m2c = new Module2Core(io_service, CoreAppConfig::Instance()->MQTT_MODULE2CORE_PORT, MQTTModule::mqtt_handler);
    m2a = new MQTTModule2Apps(io_service, m2c);
    m2c->handler = mqtt_handler;
}

MQTTModule *MQTTModule::Instance(void (*_handler)(char*)) {
    if (!module)   // Only allow one instance of class to be generated.
    {
        module = new MQTTModule(_handler);
    }
    return module;
}

void MQTTModule::mqtt_handler(char *msg) {
    //std::cout << " in mqtt_handler " << msg << std::endl;
    module->m2a->on_send_message(msg);
}

MQTTModule::~MQTTModule() {
    io_service.stop();
    delete(m2a);
    delete(m2c);
}