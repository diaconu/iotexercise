//
// Created by diaconu on 10/15/15.
//

#include "mqttmodule.h"


void handlerxx (char * msg)
{
    std::cout << " in handler " << msg << std::endl;
}

int main()
{
    MQTTModule * mod = MQTTModule::Instance(handlerxx);
    mod->run();

    return 0;
}