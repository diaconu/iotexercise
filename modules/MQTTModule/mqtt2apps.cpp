//
// Created by diaconu on 10/14/15.
//

#include "mqtt2apps.h"

MQTTModule2Apps::MQTTModule2Apps(boost::asio::io_service &_io_service, Module2Core * _m2c)
        : socket_(_io_service, udp::endpoint(udp::v4(),  0)),m2c(_m2c) {
        std::cout<< "Plugging MQTT module " <<std::endl;
        mosquitto_lib_init();
        m_mosq = mosquitto_new(NULL, true, this);

        //mosquitto_connect_async(m_mosq, BROKER_ADDR, BROKER_PORT, 60);
        connect_async(CoreAppConfig::Instance()->MQTT_BROKER_ADDR, CoreAppConfig::Instance()->MQTT_BROKER_PORT, 60);
        //mosquitto_loop_start(m_mosq);
        loop_start();
        std::cout<< "Listening for incomming MQTT messages " <<std::endl;
        //mosquitto_subscribe(m_mosq,NULL,"0",1);
        if (subscribe(NULL,"0",1) != MOSQ_ERR_SUCCESS)
        {
                std::cerr << "Could not connect to the MQTT broker. Does it run?" << std::endl;
        };

        // subscribe to the Middleware
        char send_bmessage[2048]= "alive ";
        strcat(send_bmessage, std::to_string(CoreAppConfig::Instance()->MQTT_MODULE2CORE_PORT).c_str());
        strcat(send_bmessage, " ");
        strcat(send_bmessage, CoreAppConfig::Instance()->MQTT_MODULE_ADDR);

        m2c->do_send(send_bmessage);

}

MQTTModule2Apps::~MQTTModule2Apps() {
        std::cout<< "Destroy MQTT module " <<std::endl;
        //mosquitto_loop_stop(m_mosq, true);            // Kill the thread
        loop_stop();
        mosquitto_lib_cleanup();    // Mosquitto library cleanupsocket_(io_service, udp::endpoint(udp::v4(),  CORE_MIDDLEWARE_PORT
}

void MQTTModule2Apps::on_message(const struct mosquitto_message *message) {
        std::cout << "Received message from MQTT broker: " << (char*)(message->payload) <<std::endl;
        char send_message_[2048] = "msg ";
        strncat(send_message_, (char*)message->payload, message->payloadlen);
        m2c->do_send(send_message_);
}

void MQTTModule2Apps::on_send_message(char* _message) {
        std::cout << "Send message to MQTT broker: " <<  _message << std::endl;
        //int ret = mosquitto_publish(m_mosq, NULL, "0" ,strlen(_message),_message,1,false);
        int ret = publish(NULL, "1" ,strlen(_message),_message,0,false);
        //return ( ret == MOSQ_ERR_SUCCESS );
}