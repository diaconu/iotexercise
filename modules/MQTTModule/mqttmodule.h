//
// Created by diaconu on 10/15/15.
//

#ifndef MODULES_MQTTMODULE_H
#define MODULES_MQTTMODULE_H

#include "../../config/coreappconfig.h"
#include "../common/module2core.h"
#include "mqtt2apps.h"


class MQTTModule//:public Module
{
private:

    static MQTTModule *module;

    MQTTModule(  void (*_handler)(char*));;

public:

    //attaching a handler for the messages coming from the core midleware
    //it calls the bradoadcasting through the broker
    static void mqtt_handler(char *msg);

    boost::asio::io_service io_service;
    MQTTModule2Apps *m2a;
    Module2Core * m2c;


    //boost::asio::io_service io_service_2;
    //Module2Core *m2c = new Module2Core(io_service);


    ~MQTTModule();

    void run();

    static MQTTModule * Instance(void (*_handler)(char*));

};

#endif //MODULES_MQTTMODULE_H
