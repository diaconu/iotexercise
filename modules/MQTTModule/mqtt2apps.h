//
// Created by diaconu on 10/14/15.
//

#ifndef MODULES_MQTTMODULE2APPS_H
#define MODULES_MQTTMODULE2APPS_H


#include "../../config/coreappconfig.h"
#include "../common/module2core.h"


#include <string.h>
#include <stdlib.h>
#include <iostream>

#include <mosquittopp.h>



using boost::asio::ip::udp;

class MQTTModule2Apps :public mosqpp::mosquittopp//, public Module2Apps
{
public:
    MQTTModule2Apps(boost::asio::io_service &_io_service, Module2Core * _m2c);

    ~MQTTModule2Apps();

    /*
     * function called on the object to send message to the core
     */
    void on_send_message(char* _message);;

private:
    //MQTT connection specific variables
    mosquitto *m_mosq;

    // communication woth the middleware
    udp::socket socket_;
    Module2Core * m2c;

    void on_message(const struct mosquitto_message *message);;
};

#endif //MODULES_MQTTMODULE2APPS_H
