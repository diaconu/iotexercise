# README #

Let's get these applications up and running.

### What is this repository for? ###

This repository contains the description and the source code for the programming exercise IoT researcher/developer – University of Cambridge.

### Prereqs ###

* libboost asio : 

http://www.boost.org/

http://www.boost.org/doc/libs/1_59_0/doc/html/boost_asio.html

* MosquittoMQ

http://mosquitto.org/

* RabbitMQ

https://www.rabbitmq.com/

C AMQP client library: http://alanxz.github.io/rabbitmq-c/

https://github.com/alanxz/rabbitmq-c/


### Deployment instructions ###

Each of the following folders contains a Makefile. Running the ```make``` command in each directory will produce an executable with the same name as the folder.

* coremiddleware
* modules
  * mqttmodule
  * rabbitmqmodule
* applications
  * mqttapp
  * rabbitmqapp

### How to run tests ###

You will need to open plenty of terminals. At least 5.

To run the CoreMiddleware:

```
#!sh
$ cd coremiddleware
$ ./coremiddleware

```

To run the MQTTModule, optional:

```
#!sh
$ cd modules/MQTTModule
$ ./mqttmodule

```

To run the RabbitMQModule, optional:

```
#!sh
$ cd modules/RabbbitMQModule
$ ./rabbbitmqmodule

```

NB: For now, the CoreMiddleware must be running when launching modules.

Let's run some tests with the applications:

```
#!sh
$ cd applications/mqttapp
$ ./mqttapp

```

```
#!sh
$ cd applications/rabbitmqapp
$ ./rabbbitmqapp

```

In any app terminal you can enter messages of the form:
``` number text ```
For instance 
``` 1 Down the Rabbit Hole ```
will broadcast the message 
``` 2 Down the Rabbit Hole2 ``` to everybody, including the sender.

The message flow is shown in each app, module and core middleware terminal.


### Who do I talk to? ###

* Raluca Diaconu